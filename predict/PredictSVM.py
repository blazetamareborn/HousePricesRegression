import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.feature_selection import SelectFromModel

df_train = pd.read_csv('YOUR_PATH/output/df_train.csv')
df_pred = pd.read_csv('YOUR_PATH/output/df_pred.csv')
df_target = pd.read_csv('YOUR_PATH/output/df_target.csv')

# region filtered features by Lasso
filtered_features = [
'RoofMatl_WdShngl',
'Neighborhood_StoneBr',
'Neighborhood_NridgHt',
'Neighborhood_NoRidge',
'GrLivArea',
'SaleType_New',
'OverallQual',
'Neighborhood_Crawfor',
'Exterior1st_BrkFace',
'Exterior2nd_ImStucc',
'Condition1_Norm',
'LotConfig_CulDSac',
'BsmtFinSF1',
'YearBuilt',
'LandContour_HLS',
'GarageCars',
'Neighborhood_Somerst',
'OverallCond',
'TotRmsAbvGrd',
'Functional',
'LotArea',
'WoodDeckSF',
'TotalBsmtSF',
'HouseStyle_SFoyer',
'HouseStyle_SLvl',
'ScreenPorch',
'BsmtFullBath',
'Fireplaces',
'Neighborhood_BrkSide',
'FullBath',
'BsmtFinSF2',
'LandSlope',
'LotShape_IR2',
'HeatingQC_Ex',
'PoolArea',
'Exterior1st_MetalSd',
'Foundation_PConc',
'HalfBath',
'1stFlrSF',
'BsmtHalfBath',
'ExterCond',
'MiscVal',
'SaleType_WD',
'HeatingQC_Gd',
'Exterior1st_Wd Sdng',
'EnclosedPorch',
'3SsnPorch',
'Neighborhood_Gilbert',
'OpenPorchSF',
'Neighborhood_NAmes',
'YrSold',
'Exterior1st_HdBoard',
'Neighborhood_OldTown',
'HouseStyle_1.5Fin',
'RoofStyle_Gable',
'LowQualFinSF',
'MoSold',
'SaleType_COD',
'BldgType_Twnhs',
'KitchenAbvGr',
'BedroomAbvGr',
'HouseStyle_2Story',
'SaleCondition_Abnorml',
'Neighborhood_Edwards',
'MSSubClass',
'ExterQual',
'Neighborhood_NWAmes',
'BldgType_TwnhsE',
'KitchenQual',
'LandContour_Bnk',
'Condition2_PosN',
'RoofMatl_ClyTile'
]
# endregion ()

# region filtered NEW features by Lasso
new_filtered_features = [
'AllSF-3',
'AllFlrsSF-2',
'OverallQual-s3',
'AllSF',
'SaleType_New',
'Neighborhood_NoRidge',
'YearBuilt',
'BsmtFinSF1',
'Neighborhood_NridgHt',
'Neighborhood_StoneBr',
'Condition1_Norm',
'OverallCond',
'KitchenQual-Sq',
'GarageCars-3',
'LotArea',
'Neighborhood_Crawfor',
'OverallQual-Sq',
'ExterQual-Sq',
'MSSubClass',
'Functional',
'LotConfig_CulDSac',
'KitchenAbvGr',
'Exterior1st_BrkFace',
'TotalBath-3',
'Fireplaces',
'1stFlrSF',
'Foundation_CBlock',
'ScreenPorch',
'WoodDeckSF',
'BedroomAbvGr',
'SaleCondition_Abnorml',
'Foundation_PConc',
'MoSold',
'PoolArea',
'LandContour_Bnk',
'TotRmsAbvGrd',
'Neighborhood_NWAmes',
'BsmtFinSF2',
'TotalBsmtSF',
'Neighborhood_Edwards',
'LotShape_Reg',
'OpenPorchSF',
'ExterCond',
'KitchenQual-3',
'HeatingQC_Ex',
'MSZoning_RM',
'YrSold',
'LandSlope',
'GarageScore-2',
'GarageArea',
'CentralAir',
'GarageCars-Sq',
'LotConfig_Inside',
'YearRemodAdd'
]
# endregion

X_train = df_train
y_train = df_target
X_pred = df_pred
X_pred = X_pred.drop('Id', 1)

# L1 regularization (sparse) + otomatis grid search
model = SVR(
            C=175000,
            gamma='auto'
      )

model = model.fit(X_train, y_train)

y_pred = model.predict(X_pred)

# submit Kaggle
y_pred_kaggle = model.predict(X_pred)

submission = pd.DataFrame({
    "Id": df_pred['Id'],
    "SalePrice": y_pred_kaggle
})

submission.to_csv('YOUR_PATH/output/SUBMISSION.csv', index=False)