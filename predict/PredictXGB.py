import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from xgboost.sklearn import XGBRegressor
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.feature_selection import SelectFromModel

df_train = pd.read_csv('YOUR_PATH/output/df_train.csv')
df_pred = pd.read_csv('YOUR_PATH/output/df_pred.csv')
df_target = pd.read_csv('YOUR_PATH/output/df_target.csv')

X_train = df_train
y_train = df_target
X_pred = df_pred
X_pred = X_pred.drop('Id', 1)

# L1 regularization (sparse) + otomatis grid search
model = XGBRegressor(
            n_estimators=500,
            max_depth=3,
            gamma=0,
            subsample=1,
            reg_alpha=0,
            reg_lambda=1,
            nthread=-1,
            seed=1
      )

model = model.fit(X_train, y_train)

y_pred = model.predict(X_pred)

# submit Kaggle
y_pred_kaggle = model.predict(X_pred)

submission = pd.DataFrame({
    "Id": df_pred['Id'],
    "SalePrice": y_pred_kaggle
})

submission.to_csv('YOUR_PATH/output/SUBMISSION.csv', index=False)