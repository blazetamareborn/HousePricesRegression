import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LassoCV
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.feature_selection import SelectFromModel

df_train = pd.read_csv('YOUR_PATH/output/df_train.csv')
df_pred = pd.read_csv('YOUR_PATH/output/df_pred.csv')
df_target = pd.read_csv('YOUR_PATH/output/df_target.csv')

X_train, X_test, y_train, y_test = train_test_split(df_train, df_target,
                                                    test_size=0.2, random_state=1)

# region filtered features by Lasso
filtered_features = [
'RoofMatl_WdShngl',
'Neighborhood_StoneBr',
'Neighborhood_NridgHt',
'Neighborhood_NoRidge',
'GrLivArea',
'SaleType_New',
'OverallQual',
'Neighborhood_Crawfor',
'Exterior1st_BrkFace',
'Exterior2nd_ImStucc',
'Condition1_Norm',
'LotConfig_CulDSac',
'BsmtFinSF1',
'YearBuilt',
'LandContour_HLS',
'GarageCars',
'Neighborhood_Somerst',
'OverallCond',
'TotRmsAbvGrd',
'Functional',
'LotArea',
'WoodDeckSF',
'TotalBsmtSF',
'HouseStyle_SFoyer',
'HouseStyle_SLvl',
'ScreenPorch',
'BsmtFullBath',
'Fireplaces',
'Neighborhood_BrkSide',
'FullBath',
'BsmtFinSF2',
'LandSlope',
'LotShape_IR2',
'HeatingQC_Ex',
'PoolArea',
'Exterior1st_MetalSd',
'Foundation_PConc',
'HalfBath',
'1stFlrSF',
'BsmtHalfBath',
'ExterCond',
'MiscVal',
'SaleType_WD',
'HeatingQC_Gd',
'Exterior1st_Wd Sdng',
'EnclosedPorch',
'3SsnPorch',
'Neighborhood_Gilbert',
'OpenPorchSF',
'Neighborhood_NAmes',
'YrSold',
'Exterior1st_HdBoard',
'Neighborhood_OldTown',
'HouseStyle_1.5Fin',
'RoofStyle_Gable',
'LowQualFinSF',
'MoSold',
'SaleType_COD',
'BldgType_Twnhs',
'KitchenAbvGr',
'BedroomAbvGr',
'HouseStyle_2Story',
'SaleCondition_Abnorml',
'Neighborhood_Edwards',
'MSSubClass',
'ExterQual',
'Neighborhood_NWAmes',
'BldgType_TwnhsE',
'KitchenQual',
'LandContour_Bnk',
'Condition2_PosN',
'RoofMatl_ClyTile'
]
# endregion ()

# region filtered NEW features by Lasso
new_filtered_features = [
'AllSF-3',
'AllFlrsSF-2',
'OverallQual-s3',
'AllSF',
'SaleType_New',
'Neighborhood_NoRidge',
'YearBuilt',
'BsmtFinSF1',
'Neighborhood_NridgHt',
'Neighborhood_StoneBr',
'Condition1_Norm',
'OverallCond',
'KitchenQual-Sq',
'GarageCars-3',
'LotArea',
'Neighborhood_Crawfor',
'OverallQual-Sq',
'ExterQual-Sq',
'MSSubClass',
'Functional',
'LotConfig_CulDSac',
'KitchenAbvGr',
'Exterior1st_BrkFace',
'TotalBath-3',
'Fireplaces',
'1stFlrSF',
'Foundation_CBlock',
'ScreenPorch',
'WoodDeckSF',
'BedroomAbvGr',
'SaleCondition_Abnorml',
'Foundation_PConc',
'MoSold',
'PoolArea',
'LandContour_Bnk',
'TotRmsAbvGrd',
'Neighborhood_NWAmes',
'BsmtFinSF2',
'TotalBsmtSF',
'Neighborhood_Edwards',
'LotShape_Reg',
'OpenPorchSF',
'ExterCond',
'KitchenQual-3',
'HeatingQC_Ex',
'MSZoning_RM',
'YrSold',
'LandSlope',
'GarageScore-2',
'GarageArea',
'CentralAir',
'GarageCars-Sq',
'LotConfig_Inside',
'YearRemodAdd'
]
# endregion

# region filtered final features by Lasso, remove outliers & skewness
final_filtered_features = [
'AllSF-Sq',
'OverallQual',
'Neighborhood_StoneBr',
'GarageCars',
'GrLivArea-Sq',
'OverallQual-s2',
'TotalBath',
'GrLivArea-2',
'GarageCars-3',
'SaleType_New',
'Neighborhood_NoRidge',
'Neighborhood_NridgHt',
'Neighborhood_Crawfor',
'MSZoning_C (all)',
'AllFlrsSF-Sq',
'Exterior1st_BrkFace',
'AllSF',
'Neighborhood_Mitchel',
'Neighborhood_NWAmes',
'LotConfig_CulDSac',
'LotArea',
'YearBuilt',
'Condition1_RRAe',
'TotalBath-3',
'Neighborhood_BrDale',
'HouseStyle_SLvl',
'SaleType_WD',
'MSZoning_FV',
'Neighborhood_Gilbert',
'SaleCondition_Normal',
'ExterQual-Sq',
'SaleType_COD',
'KitchenQual-Sq',
'AllSF-2',
'Condition1_Norm',
'GarageArea',
'Neighborhood_BrkSide',
'GrLivArea',
'TotalBsmtSF',
'Condition1_Artery',
'GarageScore-2',
'AllFlrsSF-2',
'GarageScore',
'BedroomAbvGr',
'Neighborhood_Edwards',
'OverallCond',
'Foundation_PConc',
'OverallGrade',
'Neighborhood_NAmes',
'BsmtFinSF1',
'KitchenAbvGr',
'LandContour_HLS',
'Exterior1st_Plywood',
'ExterQual-3',
'Functional',
'LotConfig_FR2',
'GarageQual',
'GarageCars-2',
'LandContour_Low',
'Neighborhood_CollgCr',
'Exterior1st_HdBoard',
'GarageScore-Sq',
'Exterior1st_Wd Sdng',
'KitchenQual-2',
'Neighborhood_Somerst',
'WoodDeckSF',
'PavedDrive_Y',
'Neighborhood_OldTown',
'BldgType_TwnhsE',
'TotRmsAbvGrd',
'BsmtUnfSF',
'HeatingQC_Ex',
'Fireplaces',
'YearRemodAdd',
'MSSubClass',
'ScreenPorch',
'BsmtFullBath',
'LotShape_IR2',
'CentralAir',
'2ndFlrSF',
'ExterCond',
'RoofStyle_Gable',
'SaleCondition_Abnorml',
'HouseStyle_1Story',
'GarageGrade',
'Neighborhood_SawyerW',
'EnclosedPorch',
'MSZoning_RL',
'Exterior1st_MetalSd',
'LotConfig_Corner',
'BsmtFinSF2',
'Exterior2nd_Plywood',
'YrSold',
'MiscVal',
'LandContour_Bnk',
'GarageCond',
'PoolArea',
'Condition1_RRAn',
'OpenPorchSF',
'MoSold',
'LandSlope',
'1stFlrSF',
'3SsnPorch',
'LotShape_Reg',
'RoofMatl_WdShngl',
'Exterior1st_VinylSd',
'LandContour_Lvl',
'BldgType_Duplex',
'LowQualFinSF',
'BsmtHalfBath',
'ExterGrade',
'Foundation_CBlock'
]
# endregion

# X_train = X_train[new_filtered_features]
# X_test = X_test[new_filtered_features]

# L1 regularization (sparse) + otomatis grid search
model = LassoCV(
            cv=10,
            # alphas=[0.0001, 0.0003, 0.0006, 0.001, 0.003, 0.006, 0.01, 0.03, 0.06, 0.1,
            #               0.3, 0.6, 1], # 166 dpt dr pilihan otomatis, tanpa param alphas
            max_iter=50000,
            n_jobs=-1,
            random_state=1
      )

model = model.fit(X_train, y_train)

y_pred_cv = model.predict(X_train)
y_pred_test = model.predict(X_test)

print('Best Alpha : ' + str(model.alpha_))

print('Train MAE : ' + str(mean_absolute_error(y_train, y_pred_cv)))
print('Train R^2 : ' + str(r2_score(y_train, y_pred_cv)))

print('Test R^2 : ' + str(r2_score(y_test, y_pred_test)))
print('Test MSE : ' + str(mean_squared_error(y_test, y_pred_test)))
print('Test MAE : ' + str(mean_absolute_error(y_test, y_pred_test)))

# export to excel
pd.DataFrame(y_test).to_csv('YOUR_PATH/output/y_test_lasso.csv', index=False, header=['True Value'])
pd.DataFrame(y_pred_test).to_csv('YOUR_PATH/output/y_pred_test_lasso.csv', index=False, header=['Pred'])
pd.DataFrame(X_train.columns.values).to_csv('YOUR_PATH/output/features.csv', index=False, header=['Features'])
pd.DataFrame(model.coef_).to_csv('YOUR_PATH/output/lasso_coef.csv', index=False, header=['Coef'])
